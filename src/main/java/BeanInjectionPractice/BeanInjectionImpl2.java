/*
 * Copyright (C) 2021 - 2022, Sudheer Kumar Patnana, All rights reserved.
 */

package BeanInjectionPractice;

public class BeanInjectionImpl2 implements BeanInjection {
    @Override
    public void addBean() {

    }

    @Override
    public void getBean() {

    }
}
